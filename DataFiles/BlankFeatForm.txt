PARENTHEADING: ;
FEATNAME: ;
FEATDESCRIPTION: ;
FEATTAG: ;
RACELIST: ;
CLASSLIST: ;
LEVEL: ;
ACQUIRE: ;
NEEDSALL: ;
NEEDSONE: ;
LOCK: ;
ICON: ;
VERIFIED: ;


//Options//
* Required Field
Only one of the Racelist or Classlist needs to be used.  Provide a Level and Acquire type for each Racelist then Classlist item in order in the Level and Acquire field.

PARENTHEADING: ;
*FEATNAME: ;
*FEATDESCRIPTION: ;
FEATTAG: Artificer Bonus, Rogue Bonus, Monk Bonus, Fighter Bonus, Dragonborn Bonus, Metamagic, Favored Soul Bonus, Multiple, Destiny, Past Life, Divine, Favored Enemy, Parent Feat, Half-Elf Bonus, Monk Path, Legendary, DestinyNotExclusive, Warlock Pact, Druid Shape, Cleric Bonus;
*RACELIST: Human, Elf, Halfling, Dwarf, Warforged, Drow, Half-Elf, Half-Orc, Bladeforged, Morninglord, Purple Dragon Knight, Shadar-Kai, Gnome, Deep-Gnome, Dragonborn;
*CLASSLIST: Fighter, Paladin, Barbarian, Monk, Rogue, Ranger, Cleric, Wizard, Sorcerer, Bard, Favored Soul, Artificer, Druid, Warlock;
*LEVEL: ;
*ACQUIRE: Train, Automatic;
NEEDSALL: ;
NEEDSONE: ;
LOCK: ;
*ICON: ;
VERIFIED: ;

FEATNAME: Grace of Battle;
FEATDESCRIPTION: Your faith allows you to use your Charisma bonus for to-hit and damage purposes with your deities' Favored Weapon if it is higher than your Strength bonus. This benefit only applies if your Favored Soul level is equal to or greater than half of your total Heroic Character level.;
FEATTAG: Favored Soul Bonus;
CLASSLIST: Favored Soul;
LEVEL: 2;
ACQUIRE: Train;
ICON: None;
VERIFIED: ;

FEATNAME: Knowledge of Battle;
FEATDESCRIPTION: Your faith allows you to use your Wisdom bonus for to-hit and damage purposes with your deities' Favored Weapon if it is higher than your Strength bonus. This benefit only applies if your Favored Soul level is equal to or greater than half of your total Heroic Character level.;
FEATTAG: Favored Soul Bonus;
CLASSLIST: Favored Soul;
LEVEL: 2;
ACQUIRE: Train;
ICON: None;
VERIFIED: ;

FEATNAME: Stout of Heart;
FEATDESCRIPTION: You gain 10 Hit Points per Favored Soul or Epic level.;
FEATTAG: Favored Soul Bonus;
CLASSLIST: Favored Soul;
LEVEL: 7;
ACQUIRE: Train;
ICON: None;
VERIFIED: ;

FEATNAME: Purity of Heart;
FEATDESCRIPTION: You gain 20 Spell Points per Favored Soul level, and 30 Spell Points per Epic level.;
FEATTAG: Favored Soul Bonus;
CLASSLIST: Favored Soul;
LEVEL: 7;
ACQUIRE: Train;
ICON: None;
VERIFIED: ;

